﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Runtime.Serialization.Json;
using BitbucketClient.JsonObjects;
using System.Windows.Forms;

namespace BitbucketClient
{
    /// <summary>
    /// Client to connect to BitBucket API. Currently handles Issues, but will be expanded for all data types. 
    /// </summary>
    class ApiClient
    {
        private int totalCount = 0;
        private int maxFetched = 0;


        // API is limited to currently 15 at a time. 
        private const int LIMIT = 15;

        private List<Issue> issueList;


        public ApiClient()
        {
            issueList = new List<Issue>();
        }

        /// <summary>
        /// Get all issues. First gets limit amount, then decides how many more are needed. 
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="Password"></param>
        /// <param name="OwnerID"></param>
        /// <param name="RepoName"></param>
        /// <returns></returns>
        public List<Issue> GetIssues(String UserID, String Password, String OwnerID, String RepoName)
        {

            RootObject issueRoot = GetIssues(0, LIMIT, UserID, Password, OwnerID, RepoName);

            if (issueRoot.count > LIMIT)
            {
                totalCount = issueRoot.count;
                issueList.AddRange(issueRoot.issues);
                maxFetched = LIMIT;


                while (maxFetched < totalCount)
                {
                    issueList.AddRange(GetIssues(maxFetched + 1, LIMIT, UserID, Password, OwnerID, RepoName).issues);
                    maxFetched = maxFetched + LIMIT;
                    Application.DoEvents(); 

                }

                return issueList;

            }
            else
            {

                return issueRoot.issues;
            }


        }

        /// <summary>
        /// Gets issues at a starting point, to a max of LIMIT
        /// </summary>
        /// <param name="start"></param>
        /// <param name="limit"></param>
        /// <param name="UserID"></param>
        /// <param name="Password"></param>
        /// <param name="OwnerID"></param>
        /// <param name="RepoName"></param>
        /// <returns></returns>
        public RootObject GetIssues(int start, int limit, String UserID, String Password, String OwnerID, String RepoName)
        {
            Application.DoEvents(); 
            string url = "https://api.bitbucket.org/1.0/repositories/" + OwnerID + "/" + RepoName + "/issues/?start=" + start.ToString() + "&limit=" + limit.ToString();
            var request = WebRequest.Create(url) as HttpWebRequest;

            string credentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(UserID + ":" + Password));
            request.Headers.Add("Authorization", "Basic " + credentials);

            using (var response = request.GetResponse() as HttpWebResponse)
            {
                var reader = new StreamReader(response.GetResponseStream());
                string json = reader.ReadToEnd();

                RootObject r; 
                try
                {
                    r = Deserialize<RootObject>(json);
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message + "json:" + json); 

                    throw ex; 

                }

                return r;
            }
        }



        public static string Serialize<T>(T obj)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
            MemoryStream ms = new MemoryStream();
            serializer.WriteObject(ms, obj);
            string retVal = Encoding.UTF8.GetString(ms.ToArray());
            return retVal;
        }

        public static T Deserialize<T>(string json)
        {
            
            T obj = Activator.CreateInstance<T>();
            MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(json));
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
            obj = (T)serializer.ReadObject(ms);
            ms.Close();
            return obj;
        }




    }
}
