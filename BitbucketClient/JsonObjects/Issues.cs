﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LINQtoCSV;


namespace BitbucketClient.JsonObjects
{
    public class Filter
    {
    }

    public class ReportedBy
    {
        [CsvColumn(FieldIndex = 4)]
        public string username { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public bool is_team { get; set; }
        public string avatar { get; set; }
        public string resource_uri { get; set; }

        public override string ToString()
        {
            return username;
        }
    }

    public class Responsible
    {
        [CsvColumn(FieldIndex = 6)]
        public string username { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public bool is_team { get; set; }
        public string avatar { get; set; }
        public string resource_uri { get; set; }

        public override string ToString()
        {
            return username;
        }
    }

    public class Metadata
    {
        public string kind { get; set; }
        public object version { get; set; }
        public object component { get; set; }
        public object milestone { get; set; }
    }

    public class Issue
    {

        [CsvColumn(FieldIndex = 1)]
        public string status { get; set; }

        [CsvColumn(FieldIndex = 2)]
        public string priority { get; set; }

        [CsvColumn(FieldIndex = 3)]
        public string title { get; set; }


        public ReportedBy reported_by { get; set; }

        [CsvColumn(FieldIndex = 5)]
        public string utc_last_updated { get; set; }


        public Responsible responsible { get; set; }

        [CsvColumn(FieldIndex = 7)]
        public int comment_count { get; set; }

        [CsvColumn(FieldIndex = 8)]
        public Metadata metadata { get; set; }

        [CsvColumn(FieldIndex = 9)]
        public string content { get; set; }

        [CsvColumn(FieldIndex = 10)]
        public string created_on { get; set; }

        [CsvColumn(FieldIndex = 11)]
        public int local_id { get; set; }

        [CsvColumn(FieldIndex = 12)]
        public int follower_count { get; set; }

        [CsvColumn(FieldIndex = 13)]
        public string utc_created_on { get; set; }

        [CsvColumn(FieldIndex = 14)]
        public string resource_uri { get; set; }

        [CsvColumn(FieldIndex = 15)]
        public bool is_spam { get; set; }


    }

    public class RootObject
    {
        public int count { get; set; }
        public Filter filter { get; set; }
        public object search { get; set; }
        public List<Issue> issues { get; set; }
    }
}
