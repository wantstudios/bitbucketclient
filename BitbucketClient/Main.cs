﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BitbucketClient.JsonObjects;
using System.IO;
using LINQtoCSV;

namespace BitbucketClient
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            try
            {

                button1.Text = "RETRIEVING.. PLEASE WAIT... ";
                button1.Enabled = false;

                if (String.IsNullOrEmpty(tbBBUserID.Text))
                {
                    MessageBox.Show("UserID must be completed!");
                    return;
                }

                if (String.IsNullOrEmpty(tbPassword.Text))
                {
                    MessageBox.Show("Password must be completed!");
                    return;
                }

                if (String.IsNullOrEmpty(tbOwnerID.Text))
                {
                    MessageBox.Show("OwnerID must be completed!");
                    return;
                }

                if (String.IsNullOrEmpty(tbRepoName.Text))
                {
                    MessageBox.Show("Repo Name must be completed!");
                    return;
                }



                ApiClient client = new ApiClient();
                List<Issue> issues = client.GetIssues(tbBBUserID.Text, tbPassword.Text, tbOwnerID.Text, tbRepoName.Text);


                SaveFileDialog dialog = new SaveFileDialog();
                dialog.Filter = "CSV File|*.csv";
                dialog.Title = "Save an export CSV file";
                dialog.ShowDialog();

                if (!String.IsNullOrEmpty(dialog.FileName))
                {

                    CsvFileDescription outputFileDescription = new CsvFileDescription
                    {
                        SeparatorChar = ',',
                        FirstLineHasColumnNames = true,
                        FileCultureName = "en-GB"
                    };


                    CsvContext cc = new CsvContext();

                    cc.Write(
                    issues,
                    dialog.FileName,
                    outputFileDescription);

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Theres been an error retrieving data. Ensure your details are correct, and you are connected to the internet. The exact error is: " + ex.Message); 

            }
            finally
            {


                button1.Text = "Export Issues";
                button1.Enabled = true;
            }

        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {



        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
